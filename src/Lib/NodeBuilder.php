<?php

namespace PVL\Lib;

use \Peast\Syntax\Node\Identifier;
use \Peast\Syntax\Node\Expression;

class NodeBuilder
{
	public static function buildStringLiteral($string)
	{
		$stringLiteral = new \Peast\Syntax\Node\StringLiteral();
		$stringLiteral->setValue($string);
		return $stringLiteral;
	}

	public static function buildIdentifier($id)
	{
		$identifier = new \Peast\Syntax\Node\Identifier();
		$identifier->setName($id);
		return $identifier;
	}

	public static function buildVariableDeclaration(Identifier $identifier, Expression $expression)
	{
		$variableDeclarator = new \Peast\Syntax\Node\VariableDeclarator();
		$variableDeclarator->setId($identifier);
		$variableDeclarator->setInit($expression);

		$variableDeclaration = new \Peast\Syntax\Node\VariableDeclaration();
		$variableDeclaration->setDeclarations([$variableDeclarator]);

		return $variableDeclaration;
	}

	public static function buildMemberProperty($name)
	{
		$identifier = self::buildIdentifier($name);

		$object = new \Peast\Syntax\Node\ThisExpression();

		$member = new \Peast\Syntax\Node\MemberExpression();
		$member->setProperty($identifier);
		$member->setObject($object);

		return $member;
	}

	public static function buildObjectAssignment(Expression $name, Expression $value)
	{
		$assignment = new \Peast\Syntax\Node\AssignmentExpression();

		$expression = new \Peast\Syntax\Node\ExpressionStatement();
		$expression->setExpression($assignment);

		$assignment->setLeft($name);
		$assignment->setOperator('=');
		$assignment->setRight($value);

		return $expression;
	}

	public static function buildObjectProperty(Expression $key, Expression $value)
	{
		$property = new \Peast\Syntax\Node\Property();

		$property->setKey($key);
		$property->setValue($value);

		return $property;
	}
}