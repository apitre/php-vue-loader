<?php

namespace PVL\Lib;

class FileHandler
{
	public function getContents($filename)
	{
		return file_get_contents($filename);
	}

	public function fileExists($filename)
	{
		return file_exists($filename);
	}

	public function deleteFolder($folder) 
	{ 
        if (!file_exists($folder)) { 
        	return true; 
        }
        if (!is_dir($folder) || is_link($folder)) {
            return unlink($folder);
        }
        foreach (scandir($folder) as $item) { 
            if ($item == '.' || $item == '..') { 
            	continue; 
            }
            if (!$this->deleteFolder($folder . "/" . $item, false)) { 
                chmod($folder . "/" . $item, 0777); 
                if (!$this->deleteFolder($folder . "/" . $item, false)) {
                	return false;
                }
            }; 
        } 
        return rmdir($folder); 
    }

    public function isFolder($folder)
    {
    	return is_dir($folder);
    }

    public function loadSource($filename)
    {
    	$content = $this->getContents($filename);
		return new \PVL\Vue\Source($filename, $content);
    }

	public function createFolder($folder)
	{
		if (!file_exists($folder)) {
		    mkdir($folder, 0777, true);
		}
	}

	public function putContents($filename, $content)
	{
		//echo $content;
		return file_put_contents($filename, $content);
	}
}