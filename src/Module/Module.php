<?php

namespace PVL\Module;

use \Peast\Syntax\Node\Identifier;
use \Peast\Syntax\Node\ObjectExpression;
use \Peast\Syntax\Node\VariableDeclaration;
use \Peast\Syntax\Node\VariableDeclarator;
use \Peast\Syntax\Node\FunctionExpression;
use \Peast\Syntax\Node\AssignmentExpression;
use \Peast\Syntax\Node\ThisExpression;
use \Peast\Syntax\Node\ModuleDeclaration;

use \PVL\Lib\NodeBuilder;
use \PVL\Vue\Source;

class Module
{
	private $declaration = null;
	private $identifier = null;

	public function __construct(Identifier $identifier, ModuleDeclaration $node)
	{
		$this->identifier = $identifier;
		$this->declaration = $node->getDeclaration();
	}

	public function getDeclaration(Source $source)
	{
		if ($this->declaration instanceof \Peast\Syntax\Node\FunctionDeclaration) {

			if ($source->isVue()) {
				$name = NodeBuilder::buildMemberProperty('template');
				$value = NodeBuilder::buildStringLiteral($source->getTemplate());
				$body = $this->declaration->getBody()->getbody();
				array_unshift($body, NodeBuilder::buildObjectAssignment($name, $value));
				$this->declaration->getBody()->setBody($body);
			}

			$this->declaration->setId($this->identifier);
			return $this->declaration;
		}

		if ($this->declaration instanceof \Peast\Syntax\Node\ClassDeclaration) {
			$this->declaration->setId($this->identifier);
			return $this->declaration;
		}

		if ($source->isVue()) {
			$properties = $this->declaration->getProperties();
			$key = NodeBuilder::buildIdentifier('template');
			$value = NodeBuilder::buildStringLiteral($source->getTemplate());
			array_unshift($properties, NodeBuilder::buildObjectProperty($key, $value));
			$this->declaration->setProperties($properties);
		}

		$variableDeclarator = new VariableDeclarator();
		$variableDeclarator->setId($this->identifier);
		$variableDeclarator->setInit($this->declaration);

		$variableDeclaration = new VariableDeclaration();
		$variableDeclaration->setDeclarations([$variableDeclarator]);

		return $variableDeclaration;
	}

	public function getObject()
	{
		return $this->declaration;
	}

	public function getIdentifier()
	{
		return $this->identifier;
	}
}