<?php

namespace PVL\Module;

use \Peast\Syntax\Node\Identifier;
use \Peast\Syntax\Node\ModuleDeclaration;

class ModuleList
{	
	const ALPHABET = "abcdefghijklmnopqrstuvwxyz";

	private $modules = [];
	private $indexes = [];

	public function export($filename, ModuleDeclaration $node)
	{
		if (!isset($this->indexes[$filename])) {
			$index = $this->uniqueAlphabetIndex();
			$identifier = \PVL\Lib\NodeBuilder::buildIdentifier($index);
			$this->indexes[$filename] = $index;
			$alpha = "abcdefghijklmnopqrstuvwxyz";
			$this->modules[$index] = new Module($identifier, $node);
		}
	}

	private function uniqueAlphabetIndex()
	{
		$total = count($this->indexes);

		$i = ($total % 26);

		$repeat = ($total - $i) / 26 + 1;

		return str_repeat(self::ALPHABET[$i], $repeat);
	}

	public function get($filename)
	{
		if (!isset($this->indexes[$filename])) {
			return null;
		}
		return $this->modules[$this->indexes[$filename]];
	}
}