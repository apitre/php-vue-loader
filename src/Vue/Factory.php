<?php

namespace PVL\Vue;

class Factory
{
	private static $instances = [];

	public static function getFileHandler()
	{
		return self::loadInstance('fileHandler', function(){
			return new \PVL\Lib\FileHandler();
		});
	}

	public static function getNewConfig()
	{
		return new \PVL\Vue\Config(self::getFileHandler());
	}

	public static function getLoader($config)
	{
		return self::loadInstance('loader', function() use ($config) {
			$modules = new \PVL\Module\ModuleList();
			$renderer = new \Peast\Renderer();
			$renderer->setFormatter(new \Peast\Formatter\PrettyPrint());
			//$renderer->setFormatter(new \Peast\Formatter\Compact());
			return new \PVL\Vue\Loader($config, self::getFileHandler(), $renderer, $modules);
		});
	}

	private static function loadInstance($name, $callable)
	{
		if (isset(self::$instances[$name])) {
			return self::$instances[$name];
		}

		self::$instances[$name] = call_user_func($callable);

		return self::$instances[$name];
	}
}