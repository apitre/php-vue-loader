<?php

namespace PVL\Vue;

class Source
{
	private $template = "";
	private $script = "";
	private $style = "";
	private $filename = "";

	public function __construct($filename, $content)
	{
		$this->filename = $filename;

		if ($this->isVue()) {
			$this->template = $this->getTagContents('template', $content);
			$this->style = $this->getTagContents('style', $content);
			$this->script = $this->getTagContents('script', $content);
		} else {
			$this->script = $content;
		}

		preg_replace("/\r|\n/", "", $this->template);
	}

	private function getTagContents($tagname, $string)
	{
	    preg_match("#<\s*?$tagname\b[^>]*>(.*?)</$tagname\b[^>]*>#s", $string, $matches);
	    return isset($matches[1]) ? $matches[1] : "";
	}

	public function isVue()
	{
		return strpos($this->filename, '.vue') != false;
	}

	public function getScript()
	{
		return $this->script;
	}

	public function getFilename()
	{
		return $this->filename;
	}

	public function getTemplate()
	{
		return str_replace(array("\r", "\n"), '', $this->template);
	}

	public function getStyle()
	{
		return $this->style;
	}
}