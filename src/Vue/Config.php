<?php

namespace PVL\Vue;

use \PVL\Lib\FileHandler;

class Config
{
	private $config = [];

	public function __construct(FileHandler $fileHandler)
	{
		$this->fileHandler = $fileHandler;
	}

	public function get($key)
	{
		if (!isset($this->config[$key])) {
			throw new \Exception("Invalid config index: ".$key);
		}
		return $this->config[$key];
	}

	public function init(Array $config)
	{
		$this->initNamespace($config);

		$this->initEntryPoints($config);

		$this->initDistribution($config);
	}

	private function initEntryPoints(Array $config)
	{
		if (!isset($config['entrypoints'])) {
			throw new \Exception("Missing javascript application");
		}

		if (!is_array($config['entrypoints'])) {
			throw new \Exception("Invalid javascript entry points list");
		}

		foreach ($config['entrypoints'] as $name => $entryPoint) {
			if(!$this->fileHandler->fileExists($config['namespace'].$entryPoint)) {
				throw new \Exception("Invalid javascript entry point: ".$config['namespace'].$entryPoint);
			}
		}

		$this->config['entrypoints'] = $config['entrypoints'];
	}

	private function initDistribution(Array $config)
	{
		if(!isset($config['distribution'])) {
			throw new \Exception("Missing distribution directory");
		}

		$this->config['distribution'] = $config['distribution'];
	}

	private function initNamespace(Array $config)
	{
		if(!isset($config['namespace'])) {
			throw new \Exception("Missing loader namespace");
		}

		if(!$this->fileHandler->isFolder($config['namespace'])) {
			throw new \Exception("Invalid folder: ".$config['namespace']);
		}

		$this->config['namespace'] = $config['namespace'];
	}
}