<?php

namespace PVL\Vue;

use \PVL\Vue\Vue;
use \PVL\Vue\Source;
use \PVL\Vue\Config;
use \PVL\Lib\FileHandler;
use \PVL\Lib\Helper;
use \PVL\Lib\NodeBuilder;
use \PVL\Module\ModuleList;
use \Peast\Renderer;
use \Peast\Syntax\Node\ExportDefaultDeclaration;
use \Peast\Syntax\Node\ImportDeclaration;

class Loader
{
	private $js = [];
	private $css = [];
	private $fileHandler = null;
	private $renderer = null;
	private $modulesRendered = [];
	private $modules = [];

	public function __construct(
		Config $config, 
		FileHandler $fileHandler, 
		Renderer $renderer, 
		ModuleList $modules)
	{
		$this->config = $config;
		$this->fileHandler = $fileHandler;
		$this->renderer = $renderer;
		$this->modules = $modules;
	}

	public function bundleApplications()
	{
		$time_start = microtime(true);

		$this->fileHandler->deleteFolder($this->config->get('distribution'));

		foreach ($this->config->get('entrypoints') as $name => $entryPoint) {
			$this->bundle($name, $entryPoint);
		}

		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);

		echo '<b>Bundle entire application in:</b> '.$execution_time.' Secs';
	}

	public function bundle($name, $entryPoint)
	{
		$distribution = $this->config->get('distribution');

		$bundleDir 	= "$distribution/$name";
		$bundleJs 	= "$distribution/$name/bundle.js";
		$bundleCss 	= "$distribution/$name/bundle.css";

		$this->js = [];
		$this->css = [];
		$this->modulesRendered = [];

		$source = $this->fileHandler->loadSource($this->namespace($entryPoint));

		$this->traverse($source);

		$this->fileHandler->createFolder($bundleDir);
		$this->fileHandler->putContents($bundleJs, implode('', $this->js));
		$this->fileHandler->putContents($bundleCss, implode('', $this->css));
	}

	private function traverse(Source $source)
	{
		$options = ['sourceType' => 'module'];

		$ast = \Peast\Peast::latest($source->getScript(), $options)->parse();

		$traverser = new \Peast\Traverser;

		$this->css[] = $source->getStyle();

		$traverser->addFunction(function($node) use ($source) {
			switch ($node->getType()) {
				case 'ImportDeclaration' :
					return $this->handleImportDeclaration($node);
				case 'ExportDefaultDeclaration' :
					return $this->handleExportDefaultDeclaration($source, $node);
			}
		});

		$traverser->traverse($ast);

		$this->js[] = $this->renderer->render($ast);
	}

	private function handleExportDefaultDeclaration(Source $source, ExportDefaultDeclaration $node)
	{
		$this->modules->export($source->getFilename(), $node);

		return \Peast\Traverser::REMOVE_NODE;
	}

	private function namespace($import)
	{	
		if (strpos($import, '.') === false) {
			$import = "$import.js";
		}

		if ($import[0] != '/') {
			$import = "/$import";
		}

		return $this->config->get('namespace').$import;
	}

	private function handleImportDeclaration(ImportDeclaration $node)
	{
		$import = $node->getSource()->getValue();

		$source = $this->fileHandler->loadSource($this->namespace($import));

    	$filename = $source->getFilename();

    	$this->traverse($source);

    	$module = $this->modules->get($filename);

    	if (!in_array($filename, $this->modulesRendered)) {
    		$this->js[] = $this->renderer->render($module->getDeclaration($source)).";";
    		$this->modulesRendered[] = $filename;
    	}

    	$specifier = $node->getSpecifiers()[0];

		return NodeBuilder::buildVariableDeclaration($specifier->getLocal(), $module->getIdentifier());
	}
}