<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./dist/main/bundle.css">
	<script src="./assets/vue/vue.js"></script>
	<script src="./dist/main/bundle.js"></script>
</head>
<body>
	<div id="main"></div>
</body>
</html>