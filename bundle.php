<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("__ROOT__", $_SERVER['DOCUMENT_ROOT']);

require __ROOT__.'/vendor/autoload.php';

$config = \PVL\Vue\Factory::getNewConfig();

$config->init([
	'namespace' => __ROOT__.'/assets',
	'distribution' => __ROOT__.'/dist',
	'entrypoints' => [
		'test' => '/test/index.js',
		'main' => '/main.js'
	]
]);

$loader = \PVL\Vue\Factory::getLoader($config);

$loader->bundleApplications();