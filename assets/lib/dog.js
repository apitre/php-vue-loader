export default function(name, color)
{
	var _this = {};

	_this.name = name;
	_this.color = color;

	this.describe = function()
	{
		return "type: Dog name: " + _this.name + " color: " + _this.color;
	}
}