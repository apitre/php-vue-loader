import tomate from 'alain';
import test from 'alain';
import App from '/components/main.vue';
import Dog from '/lib/dog.js';

var app = null;

document.addEventListener('DOMContentLoaded', function(){
	var dog = new Dog('Bernard', 'brown');
	app = new App(dog);
	var c = new tomate();
	c.test();
}, false);
